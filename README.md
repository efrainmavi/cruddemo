# employee-rest-demo

The application provides a REST API that allows basic operations as:

    - Create new employees
    - Update existing employees
    - Delete employees
    - Get all employees
    - Get employees by an ID
    
The Employees follow the below specification:

    ID - Unique identifier for an employee
    FirstName - Employees first name
    MiddleInitial - Employees middle initial
    LastName - Employeed last name
    DateOfBirth - Employee birthday and year
    DateOfEmployment - Employee start date
    Status - ACTIVE or INACTIVE

# Technical Description

The application was made on the Spring Initializr tool to create a skeleton
for Spring Boot app added the depencies:

    - Spring Web
        - Include Restful, Spring MVC
        - Apache Tomcat as embedded server, etc.
    - Spring Data JPA
        - Java Pesistence API
        - H2 embedded Database
    - Spring Security
        - Json Web Token (JWT)
    - Spring Fox
        - Swagger Documentation API
        - Swagger UI
        
**Architectural pattern MVC**: This was chosen because it makes a perfect fit for this type of applications allowing to easily separate the view form all the logic behind.
**Design Pattern Singleton**: By default spring beans and controllers are singleton, on an API that is receiving many calls this helps by reducing resources usage in the application, due only uses one instance of a class at a time.

# Usage

### AWS cloud deployed service 

The application is already deployed on cloud using of the service of AWS `Elastick Beanstalk`, to interact with the application on cloud go to this link to see the API documentation

[Employee REST API](http://rest-api-employee-dev.ufji9g2nw6.us-east-1.elasticbeanstalk.com/swagger-ui.html)

### Clone the repository

Run the following command:

    git clone https://gitlab.com/efrainmavi/employee-rest-demo.git

once you already clone the project you will find a file application.properties under "/src/main/java/resources",  this file is empty, you should add the following properties :


H2 properties value
    
```
    spring.jpa.hibernate.ddl-auto=create-drop
    spring.datasource.url=jdbc:h2:mem:testdb
    spring.datasource.driverClassName=org.h2.Driver
    spring.datasource.username=sa
    spring.datasource.password=
```
  Spring Security

```
jwt.id=youjwtid
jwt.secret=yourjwtsecret
spring.security.user.name=yourusername
spring.security.user.password=yourpassword
```
   **Note:** For the security properties can be anything.
   
### How to start the application

To run the application simply run the `gradle bootRun` command on the command line at the root directory of the application.
   
   ### Interact with the application
   
   Once the application has successfully started should be available at [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html "http://localhost:8080/swagger-ui.html"), due is a swagger API documentation implemented which allows to interact with the API, from there the resource methods are expose at `/api/v1/employees`.
   
   #### Examples
   
   Retrieve all employees 
   
   GET http://localhost:8080/api/v1/employees
   
   Expected result:
   ```json
[
  {
    "id": 1,
    "firstName": "Leslie",
    "middleInitial": "G.",
    "lastName": "Andrews",
    "dateOfBirth": "1989-08-29",
    "dateOfEmployment": "2009-08-29",
    "status": true
  },
  {
    "id": 2,
    "firstName": "Emma",
    "middleInitial": "M.",
    "lastName": "Baumgarten",
    "dateOfBirth": "1992-07-02",
    "dateOfEmployment": "2011-04-20",
    "status": true
  },
  {
    "id": 3,
    "firstName": "Avani",
    "middleInitial": "K.",
    "lastName": "Gupta",
    "dateOfBirth": "1990-04-12",
    "dateOfEmployment": "2010-05-15",
    "status": true
  }
]
```
Retrieve employee by ID

GET [http://localhost:8080/api/v1/employees/{id}](http://localhost:8080/api/v1/employees/{id})

Expected result:

```json
{
  "id": 1,
  "firstName": "Leslie",
  "middleInitial": "G.",
  "lastName": "Andrews",
  "dateOfBirth": "1989-08-29",
  "dateOfEmployment": "2009-08-29",
  "status": true
}
```
Create Employee

POST [http://localhost:8080/employees/](http://localhost:8080/employees/)

Json to send
   
   
   ```json
{
  "id": 0,
  "firstName": "Maribel",
  "lastName": "Roman",
  "middleInitial": "E.",
  "dateOfBirth": "1989-08-29",
  "dateOfEmployment": "1991-06-10",
  "status": true
}
```
Update Employee

PUT [http://localhost:8080/api/v1/employees/{id}](http://localhost:8080/api/v1/employees/{id})

Json to send

```json
{
    "id": 0,
    "firstName": "Sofia",
    "middleInitial": "I.",
    "lastName": "Martinez",
    "dateOfBirth": "1992-07-02",
    "dateOfEmployment": "2011-04-20",
    "status": true
  }
```
Delete Employee

DELETE [http://localhost:8080/api/v1/employees/{id}](http://localhost:8080/api/v1/employees/{id})

This call uses Authorization bearer Token, like the below example.
```
Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJ0b2tlbklkIiwic3ViIjoiYWRtaW4iLCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwiaWF0IjoxNTczODc1NTA1LCJleHAiOjE1NzM4NzYxMDV9.7AWWGlXIzvd3n7zrznHmk4poiQ5UWL2YpZcFRgGNV_4kZQOYTdPX0vyjaVTbXpJrJ0pLbFjHVWTUZ0vSTVlBfQ
```
for generate it is need to make a post call to the following resource:

POST http://localhost:8080/api/v1/token

This call uses Authentication Basic, the following properties credentials can be used:

 - **User :** `spring.security.user.name`

 - **Password :** `spring.security.user.name`
 
 **Note :** to get successfuly the call this it should be pass a parameter the user name and the password incase that is used Postman or soapUI
 
 ## Pre-loading users in the application
 
 The application pre-load some data in the DB after it has started. This data is contained on a json file called employees.json located under src/main/resources/, following the below structure.
 
 ```json
[
   {
    "id": 1,
    "firstName": "Leslie",
    "middleInitial": "G.",
    "lastName": "Andrews",
    "dateOfBirth": "1989-08-29",
    "dateOfEmployment": "2009-08-29",
    "status": true
   },
   {
    "id": 2,
    "firstName": "Emma",
    "middleInitial": "M.",
    "lastName": "Baumgarten",
    "dateOfBirth": "1992-07-02",
    "dateOfEmployment": "2011-04-20",
    "status": true
   }
]
```
   
   
   
   
   
   
   
   
   
   
   
   
   