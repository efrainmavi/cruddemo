package com.rest.employee.cruddemo.controllertest;

import com.rest.employee.cruddemo.controller.EmployeeController;
import com.rest.employee.cruddemo.controller.TokenController;
import com.rest.employee.cruddemo.model.Employee;
import com.rest.employee.cruddemo.service.EmployeeService;
import com.rest.employee.cruddemo.service.TokenService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest({EmployeeController.class, TokenController.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ControllerTest {

    private final static String CLASS_NAME = ControllerTest.class.getName();
    private final static Logger LOGGER = LogManager.getLogger(CLASS_NAME);

    private static final String RETRIEVE_EMPLOYEES = "[" +
            "{\"id\":1,\"firstName\":\"Leslie\",\"middleInitial\":\"G.\",\"lastName\":\"Andrews\"," +
            "\"dateOfBirth\":\"1989-08-29\",\"dateOfEmployment\":\"2009-08-29\",\"status\":true}," +
            "{\"id\":2,\"firstName\":\"Emma\",\"middleInitial\":\"M.\",\"lastName\":\"Baumgarten\"," +
            "\"dateOfBirth\":\"1992-07-02\",\"dateOfEmployment\":\"2011-04-20\",\"status\":true}," +
            "{\"id\":3,\"firstName\":\"Avani\",\"middleInitial\":\"K.\",\"lastName\":\"Gupta\"," +
            "\"dateOfBirth\":\"1990-04-12\",\"dateOfEmployment\":\"2010-05-15\",\"status\":true}" +
            "]";

    private static final String CREATE_EMPLOYEE = "{\"id\":1,\"firstName\":\"EmployeeTest\",\"middleInitial\":\"E.\",\"lastName\":\"EmployeeTestLastName\","
            + "\"dateOfBirth\":\"1991-06-10\",\"dateOfEmployment\":\"2019-05-16\",\"status\":true}";

    private static final String UPDATE_EMPLOYEE = "{\"id\":2,\"firstName\":\"EmployeeUpdateTest\",\"middleInitial\":\"U.\",\"lastName\":\"EmployeeUpdateTest\","
            + "\"dateOfBirth\":\"1991-06-10\",\"dateOfEmployment\":\"2019-05-16\",\"status\":true}";

    private static final String EXPECTED_TOKEN_RESPONSE = "{\"userName\":\"userTest\",\"password\":null,\"token\":\"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9\"}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    EmployeeService employeeService;

    @MockBean
    TokenService tokenService;

    @Before
    public void init() {

        Employee emp = new Employee();
        emp.setId(1);
        emp.setFirstName("EmployeeTest");
        emp.setMiddleInitial("E.");
        emp.setLastName("EmployeeTestLastName");
        emp.setDateOfBirth(LocalDate.of(1991, Month.JUNE, 10));
        emp.setDateOfEmployment(LocalDate.of(2019, Month.MAY, 16));
        emp.setStatus(true);

        Employee updateEmployee = new Employee();
        updateEmployee.setId(2);
        updateEmployee.setFirstName("EmployeeUpdateTest");
        updateEmployee.setMiddleInitial("U.");
        updateEmployee.setLastName("EmployeeUpdateTest");
        updateEmployee.setDateOfBirth(LocalDate.of(1991, Month.JUNE, 10));
        updateEmployee.setDateOfEmployment(LocalDate.of(2019, Month.MAY, 16));
        updateEmployee.setStatus(true);

        List<Employee> retrieveEmployees = new ArrayList<>();
        retrieveEmployees.add(new Employee(1, "Leslie", "G.", "Andrews", LocalDate.of(1989, Month.AUGUST, 29),
                LocalDate.of(2009, Month.AUGUST, 29), true));
        retrieveEmployees.add(new Employee(2, "Emma", "M.", "Baumgarten", LocalDate.of(1992, Month.JULY, 02),
                LocalDate.of(2011, Month.APRIL, 20), true));
        retrieveEmployees.add(new Employee(3, "Avani", "K.", "Gupta", LocalDate.of(1990, Month.APRIL, 12),
                LocalDate.of(2010, Month.MAY, 15), true));

        Mockito.when(employeeService.getAllEmployees()).thenReturn(retrieveEmployees);
        Mockito.when(employeeService.createEmployee(Mockito.any(Employee.class))).thenReturn(emp);
        Mockito.when(employeeService.getEmployeeById(emp.getId())).thenReturn(java.util.Optional.of(emp));
        Mockito.when(employeeService.getEmployeeById(updateEmployee.getId())).thenReturn(java.util.Optional.of(updateEmployee));
        Mockito.when(employeeService.updateEmployee(Mockito.any(Employee.class))).thenReturn(updateEmployee);
        Mockito.when(employeeService.deleteEmployee(emp)).thenReturn(emp);

        // Mocking the token service for test the token endpoint
        Mockito.when(tokenService.getJWToken("userTest", "pwdTest")).thenReturn("Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9");


    }

    @Test
    public void retrieveEmployeesTest() throws Exception {
        LOGGER.traceEntry("retrieveEmployeesTest()");
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/employees")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        String actualResult = mvcResult.getResponse().getContentAsString();

        Assert.assertEquals(RETRIEVE_EMPLOYEES, actualResult);

        LOGGER.info("\nRetrieve all Employees test result" +
                "\nExpected Result: " + RETRIEVE_EMPLOYEES + "\n" +
                "Actual Result: " + actualResult);
    }

    @Test
    public void createEmployeeTest() throws Exception {

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .post("/api/v1/employees")
                .content(CREATE_EMPLOYEE)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        String actualResult = mvcResult.getResponse().getContentAsString();

        Assert.assertEquals(CREATE_EMPLOYEE, actualResult);
        LOGGER.info("\nCreate Employee test result" +
                "\nExpected Result: " + CREATE_EMPLOYEE + "\n" +
                "Actual Result: " + actualResult);
    }

    @Test
    public void retrieveEmployeeByIdTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/employees/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        String actualResult = mvcResult.getResponse().getContentAsString();

        Assert.assertEquals(CREATE_EMPLOYEE, actualResult);

        LOGGER.info("\nRetrieve Employee by id test result" +
                "\nExpected Result: " + CREATE_EMPLOYEE + "\n" +
                "Actual Result: " + actualResult);
    }

    @Test
    public void updateEmployeeTest() throws Exception {

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .put("/api/v1/employees/{id}", 1)
                .content(UPDATE_EMPLOYEE)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        String actualResult = mvcResult.getResponse().getContentAsString();

        Assert.assertEquals(UPDATE_EMPLOYEE, actualResult);
        LOGGER.info("\nUpdate Employee test result" +
                "\nExpected Result: " + UPDATE_EMPLOYEE + "\n" +
                "Actual Result: " + actualResult);
    }

    @Test
    public void deleteEmployeeWithoutTokenTest() throws Exception {
        int expectedResult = 403;
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/v1/employees/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden()).andReturn();
        int actualResult = mvcResult.getResponse().getStatus();

        Assert.assertEquals(expectedResult, actualResult);

        LOGGER.info("\nDelete Employee without token test result" +
                "\nExpected Result: " + expectedResult + "\n" +
                "Actual Result: " + actualResult);
    }

    //Unit test for generate the bearer token
    @Test
    public void retrieveTokenTest() throws Exception {

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .post("/api/v1/token")
                .param("userName", "userTest")
                .param("pwd", "pwdTest")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        String actualResult = mvcResult.getResponse().getContentAsString();

        Assert.assertEquals(EXPECTED_TOKEN_RESPONSE, actualResult);

        LOGGER.info("\nRetrieve Token result" +
                "\nExpected Result: " + EXPECTED_TOKEN_RESPONSE + "\n" +
                "Actual Result: " + actualResult);
    }
}