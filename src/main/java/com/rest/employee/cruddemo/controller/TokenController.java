package com.rest.employee.cruddemo.controller;

import com.rest.employee.cruddemo.dto.UserDTO;
import com.rest.employee.cruddemo.service.TokenService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class TokenController {

    private final static String CLASS_NAME = TokenController.class.getName();
    private final static Logger LOGGER = LogManager.getLogger(CLASS_NAME);

    @Autowired
    TokenService tokenService;

    /**
     * This method is to generate a bearer token
     *
     * @param userName is the user name who is requesting for a token
     * @param pwd      is the password associate to the userName parameter
     * @return the user with the generated token
     */
    @PostMapping(value = "/token", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> retrieveToken(@RequestParam String userName, @RequestParam String pwd) {
        String METHOD_NAME = "retrieveToken()";
        LOGGER.traceEntry(METHOD_NAME);
        String token = tokenService.getJWToken(userName, pwd);
        UserDTO user = new UserDTO();
        user.setUserName(userName);
        user.setToken(token);

        return ResponseEntity.ok(user);
    }
}