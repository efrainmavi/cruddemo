package com.rest.employee.cruddemo.controller;

import com.rest.employee.cruddemo.exceptions.EmployeeExceptionResponse;
import com.rest.employee.cruddemo.model.Employee;
import com.rest.employee.cruddemo.service.EmployeeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(EmployeeController.EMPLOYEE_ENDPOINT)
public class EmployeeController {

    private final static String CLASS_NAME = EmployeeController.class.getName();
    private final static Logger LOGGER = LogManager.getLogger(CLASS_NAME);

    public final static String EMPLOYEE_ENDPOINT = "/api/v1/employees";

    @Autowired
    private EmployeeService employeeService;

    /**
     * This method allow us to retrieve the employee by Id.
     *
     * @param id this is the employee id
     * @return the saved employee with the id that we passed and status 200 if the employee is found.
     * @throws EmployeeExceptionResponse thrown a custom exception with a not found message and logged it with an error log.
     */
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> retrieveEmployeeById(@Valid @PathVariable("id") Integer id) {

        String METHOD_NAME = "getEmployeeById()";
        LOGGER.traceEntry(METHOD_NAME);

        Optional<Employee> optionalEmployee = employeeService.getEmployeeById(id);
        if (optionalEmployee.isPresent()) {
            LOGGER.info(optionalEmployee.get());
            return ResponseEntity.ok(optionalEmployee.get());
        } else {
            String mgs = "The Employee with this  " + id + " id was not found";
            LOGGER.error(mgs);
            throw new EmployeeExceptionResponse(mgs);
        }
    }

    /**
     * This method allow us to create new employees and validate its attributes to avoid null or wrong values
     *
     * @param employee is the object representation of the employee that will save it.
     * @param result
     * @return the saved Employee in a json format.
     * @throws Exception if the Employee is null and logged it with a error log.
     */
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createEmployee(@Valid @RequestBody Employee employee, BindingResult result) throws Exception {

        String METHOD_NAME = "createEmployee()";
        LOGGER.traceEntry(METHOD_NAME);

        if (!employee.equals(null)) {
            LOGGER.info(employee.toString());
            return ResponseEntity.ok(employeeService.createEmployee(employee));
        } else {
            String mgs = "The employee was not created successfully";
            LOGGER.error(mgs, employee.toString());
            throw new Exception(mgs);
        }
    }

    /**
     * This method allow us to update the Employee that is already is saved.
     *
     * @param employee is the object representation of the employee that will be update.
     * @param id       is the employee id.
     * @return the updated Employee in a json format
     * @throws EmployeeExceptionResponse thrown a custom exception if the employee isn't found and logged it with
     *                                   error log.
     */
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateEmployee(@Valid @RequestBody Employee employee, @PathVariable("id") Integer id) {

        String METHOD_NAME = "updateEmployee()";
        LOGGER.traceEntry(METHOD_NAME);

        Optional<Employee> optionalEmployee = employeeService.getEmployeeById(id);
        if (optionalEmployee.isPresent()) {
            employee.setId(id);
            return ResponseEntity.ok(employeeService.updateEmployee(employee));
        } else {
            String mgs = "Unable to update the Employee with this Id " + id;
            LOGGER.error(mgs);
            throw new EmployeeExceptionResponse(mgs);
        }
    }

    /**
     * This method allow us to delete the employees by id, this is not a physical delete it is a logical delete instead
     * this process consist in switch the status of the employee from "ACTIVE" to "INACTIVE"
     *
     * @param id is the employee id
     * @return not content response
     * @throws EmployeeExceptionResponse thrown a custom exception if the employee isn't found and logged it with
     *                                   error log.
     */
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteEmployee(@PathVariable("id") Integer id) {

        String METHOD_NAME = "deleteEmployee()";
        LOGGER.traceEntry(METHOD_NAME);

        Optional<Employee> optionalEmployee = employeeService.getEmployeeById(id);
        if (optionalEmployee.isPresent()) {
            employeeService.deleteEmployee(optionalEmployee.get());
            return ResponseEntity.noContent().build();
        } else {
            String mgs = "The Employee with this  " + id + " id was not found";
            LOGGER.error(mgs);
            throw new EmployeeExceptionResponse(mgs);
        }
    }

    /**
     * This method retrieve all the deleted employees
     *
     * @return a list of the deleted employees
     * @throws EmployeeExceptionResponse thrown a custom exception when the list of employees is empty or null
     *                                   this happens when there is not deleted employees.
     */
    @GetMapping(value = "/deleted", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> retrieveAllDeletedEmployees() {

        String METHOD_NAME = "getDeletedEmployees()";
        LOGGER.traceEntry(METHOD_NAME);

        List<Employee> allEmployees = employeeService.getDeletedEmployees();
        if (!allEmployees.isEmpty()) {
            return ResponseEntity.ok(allEmployees);
        } else {
            String mgs = "Employees not found";
            LOGGER.error(mgs);
            throw new EmployeeExceptionResponse(mgs);
        }
    }

    /**
     * This method retrieve all the employees that are saved in the DB
     *
     * @return a list of employees.
     * @throws EmployeeExceptionResponse thrown a custom exception if the list is empty or null
     *                                   this happen when there is any employee saved in the DB
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> retrieveAllEmployees() {
        String METHOD_NAME = "getAllEmployees()";
        LOGGER.traceEntry(METHOD_NAME);

        List<Employee> allEmployees = employeeService.getAllEmployees();
        if (!allEmployees.isEmpty()) {
            return ResponseEntity.ok(allEmployees);
        } else {
            String mgs = "Employees not found";
            LOGGER.error(mgs);
            throw new EmployeeExceptionResponse(mgs);
        }
    }
}