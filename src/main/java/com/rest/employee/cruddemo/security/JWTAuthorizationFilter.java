package com.rest.employee.cruddemo.security;

import io.jsonwebtoken.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class JWTAuthorizationFilter extends OncePerRequestFilter {

    private final static String CLASS_NAME = JWTAuthorizationFilter.class.getName();
    private final static Logger LOGGER = LogManager.getLogger(CLASS_NAME);

    private final String HEADER = "Authorization";
    private final String PREFIX = "Bearer ";

    private String secretKey;

    public JWTAuthorizationFilter(String secret) {
        this.secretKey = secret;
    }

    /**
     * This method set up the token.
     *
     * @param request
     * @param response
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        LOGGER.traceEntry("doFilterInternal()");
        try {
            if (verifyJWTToken(request, response)) {

                Claims claims = validateToken(request);
                if (claims.get("authorities") != null) {
                    setAuthenticationToken(claims);
                } else {
                    SecurityContextHolder.clearContext();
                }
            }
            filterChain.doFilter(request, response);

        } catch (ExpiredJwtException ex) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            LOGGER.error(ex.getMessage(), ex);

        } catch (UnsupportedJwtException | MalformedJwtException ex) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_FORBIDDEN, ex.getMessage());
            LOGGER.error(ex.getMessage(), ex);
        }
        LOGGER.traceExit("doFilterInternal()");
    }

    /**
     * Set the authentication process
     *
     * @param claims
     */
    private void setAuthenticationToken(Claims claims) {
        LOGGER.traceEntry("setAuthenticationToken()");

        @SuppressWarnings(value = "unchecked")
        List<String> authorities = (List<String>) claims.get("authorities");
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(claims.getSubject(), null,
                authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
        SecurityContextHolder.getContext().setAuthentication(auth);

        LOGGER.traceExit("setAuthenticationToken()");
    }

    /**
     * Validate the generated token
     *
     * @param request
     * @return
     */
    private Claims validateToken(HttpServletRequest request) {
        LOGGER.traceEntry("validateToken()");

        String jwtToken = request.getHeader(HEADER).replace(PREFIX, "");
        return Jwts.parser().setSigningKey(secretKey.getBytes()).parseClaimsJws(jwtToken).getBody();
    }

    /**
     * Verify the token in the headers
     *
     * @param request
     * @param response
     * @return
     */
    private boolean verifyJWTToken(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.traceEntry("verifyJWTToken()");

        String authenticationHeader = request.getHeader(HEADER);
        if (authenticationHeader == null || !authenticationHeader.startsWith(PREFIX)) {
            return false;
        } else {
            return true;
        }
    }
}