package com.rest.employee.cruddemo.config;

import com.google.gson.*;
import com.rest.employee.cruddemo.model.Employee;
import com.rest.employee.cruddemo.repository.EmployeeRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDate;

@Configuration
public class LoadData {

    private final static String CLASS_NAME = LoadData.class.getName();
    private final static Logger LOGGER = LogManager.getLogger(CLASS_NAME);

    /**
     * This method load the employees into the embedded database from employees.json
     *
     * @param employeeRepository
     * @return
     */
    @Bean
    public CommandLineRunner loadDatabase(EmployeeRepository employeeRepository) {
        String METHOD_NAME = "runner()";
        LOGGER.traceEntry("Entering to " + METHOD_NAME);

        GsonBuilder builder = new GsonBuilder();

        return args -> {
            Gson parser = builder.registerTypeAdapter(LocalDate.class, new JsonDeserializer<LocalDate>() {
                @Override
                public LocalDate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                    return LocalDate.parse(json.getAsJsonPrimitive().getAsString());
                }
            }).create();

            File file = ResourceUtils.getFile("classpath:employees.json");
            try (FileReader reader = new FileReader(file)) {

                JsonArray employeeArray = parser.fromJson(reader, JsonArray.class);
                employeeArray.forEach(emp -> employeeRepository.save(parser.fromJson(emp.getAsJsonObject(), Employee.class)));

                LOGGER.info("Employees saved");

            } catch (FileNotFoundException ex) {
                LOGGER.error("Error while loading the data " + ex.getMessage());
            } catch (IOException ex) {

                LOGGER.error("Unable to save the users " + ex.getMessage());
            }

            LOGGER.traceExit("Exiting from " + METHOD_NAME);
        };
    }
}
