package com.rest.employee.cruddemo.config;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Primary
public class SwaggerConfig {


    @Autowired
    private TypeResolver typeResolver;

    /**
     * This method allow to create all the related swagger API documentation
     *
     * @return
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Kenzan - Employee Service")
                .apiInfo(apiInfo())
                .enable(true)
                .select().paths(PathSelectors.regex("/api/v1.*"))
                .build();
    }

    /**
     * This method allow to put customized information description for the API
     *
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Kenzan Service")
                .description("REST API to create, get, update and delete Employees")
                .version("1.0.0")
                .license("Apache License Version 2.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
                .contact(new Contact("Efrain Martinez", "www.amdocs.com", "efrainmavi@gmail.com"))
                .build();
    }
}
