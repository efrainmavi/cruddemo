package com.rest.employee.cruddemo.repository;

import com.rest.employee.cruddemo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    public List<Employee> getEmployeesByStatus(Boolean status);

    Optional<Employee> findById(Integer id);
}