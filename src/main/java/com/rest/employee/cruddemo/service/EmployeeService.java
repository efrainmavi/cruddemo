package com.rest.employee.cruddemo.service;

import com.rest.employee.cruddemo.model.Employee;
import com.rest.employee.cruddemo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    /**
     * This method is for find the employee by id
     * @param id is the employee id
     * @return the employee with the associated id
     */
    public Optional<Employee> getEmployeeById(Integer id){
        return employeeRepository.findById(id);
    }

    /**
     * This method is for save the employee on our DB
     *
     * @param employee is the object that will be save in the DB.
     * @return the employee saved in the DB.
     */
    public Employee createEmployee(Employee employee){
        return employeeRepository.save(employee);
    }

    /**
     * This method will update an employee saved.
     *
     * @param employee the employee with the attributes to update.
     * @return the updated employee.
     */
    public Employee updateEmployee(Employee employee){

        return employeeRepository.save(employee);
    }

    /**
     * This method is for approach a logic delete instead of physical delete
     *
     * @param employee is the employee that will be delete from the DB.
     * @return the deleted employee.
     */
    public Employee deleteEmployee(Employee employee){
        employee.setStatus(false);
        return employeeRepository.save(employee);
    }

    /**
     * This method is to retrieve all the employees in the DB.
     *
     * @return a list of employees
     */
    public List<Employee> getAllEmployees(){
        return employeeRepository.getEmployeesByStatus(true);
    }

    /**
     * This method is to retrieve all the deleted employees.
     *
     * @return a list of employees
     */
    public List<Employee> getDeletedEmployees(){
        return employeeRepository.getEmployeesByStatus(false);
    }
}
