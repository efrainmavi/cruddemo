package com.rest.employee.cruddemo.service;

import com.rest.employee.cruddemo.exceptions.AccessDeniedExceptionResponse;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TokenService {
    private final static String CLASS_NAME = TokenService.class.getName();
    private final static Logger LOGGER = LogManager.getLogger(CLASS_NAME);

    @Value("${spring.security.user.name}")
    private String user;
    @Value("${spring.security.user.password}")
    private String pwd;
    @Value("${jwt.id}")
    private String tokenId;
    @Value("${jwt.secret}")
    private String secretKey;

    public String getJWToken(String userName, String password) {
        String METHOD_NAME = "getJWToken()";
        LOGGER.traceEntry(METHOD_NAME);

        if (user.equals(userName) && pwd.equals(password)) {

            List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");
            String token = Jwts.builder().setId(tokenId).setSubject(userName).claim("authorities",
                    grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                    .setIssuedAt(new Date(System.currentTimeMillis()))
                    .setExpiration(new Date(System.currentTimeMillis() + 600000))
                    .signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();
            return "Bearer " + token;
        } else {
            String mgs = "User or Password are invalid.";
            LOGGER.error("AccessDenied Exception was thrown, " + mgs);
            throw new AccessDeniedExceptionResponse(mgs);
        }
    }
}