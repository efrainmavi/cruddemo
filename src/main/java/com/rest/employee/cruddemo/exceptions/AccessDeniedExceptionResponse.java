package com.rest.employee.cruddemo.exceptions;

public class AccessDeniedExceptionResponse extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AccessDeniedExceptionResponse(String mgs) {
        super(mgs);
    }
}