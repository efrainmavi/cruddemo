package com.rest.employee.cruddemo.exceptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.Set;

@ControllerAdvice
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

    private final static String CLASS_NAME = ResponseExceptionHandler.class.getName();
    private final static Logger LOGGER = LogManager.getLogger(CLASS_NAME);

    /**
     * This method is to handle all the exceptions related with the EmployeeExceptionResponse for NotFoundException.
     *
     * @param ex      is the custom exception that was thrown previously and contain the custom message.
     * @param request contain the details of the http request from the controller.
     * @return the final details of the exception with the status code.
     */
    @ExceptionHandler({EmployeeExceptionResponse.class})
    public final ResponseEntity<Object> handleEmployeeNotFoundException(EmployeeExceptionResponse ex, WebRequest request) {
        String METHOD_NAME = "handleEmployeeNotFoundException()";
        LOGGER.traceEntry(METHOD_NAME);
        ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<Object>(errorDetails, HttpStatus.NOT_FOUND);
    }

    /**
     * This method is to handle the exceptions related with create employees request.
     *
     * @param ex      is the exception that was thrown, due a validation related.
     * @param request contain the details of the http request from the controller.
     * @return the final details of the exception with the status code and the validation details.
     */
    @ExceptionHandler({Exception.class})
    public final ResponseEntity<Object> handleCreateEmployeeException(ConstraintViolationException ex, WebRequest request) {
        String METHOD_NAME = "handleCreateEmployeeException()";
        LOGGER.traceEntry(METHOD_NAME);

        Set<ConstraintViolation<?>> violationExceptions = ex.getConstraintViolations();
        StringBuilder exceptionMessage = new StringBuilder();
        violationExceptions.forEach(constraintViolation -> exceptionMessage.append(constraintViolation.getMessage()));

        ErrorDetails errorDetails = new ErrorDetails(new Date(), exceptionMessage.toString(), request.getDescription(false));

        LOGGER.error(exceptionMessage.toString(), ex);
        return new ResponseEntity<Object>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    /**
     * This method is to handle all the access denied exceptions related with the security.
     *
     * @param ex      is the exception that was thrown related to authentication operation.
     * @param request contain the details of the http request from the controller.
     * @return the final details of the exception with the status code.
     */
    @ExceptionHandler({AccessDeniedExceptionResponse.class})
    public final ResponseEntity<Object> handleAccessDeniedException(AccessDeniedExceptionResponse ex, WebRequest request) {
        String METHOD_NAME = "handleAccessDeniedException()";
        LOGGER.traceEntry(METHOD_NAME);

        ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));

        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<Object>(errorDetails, HttpStatus.UNAUTHORIZED);
    }
}