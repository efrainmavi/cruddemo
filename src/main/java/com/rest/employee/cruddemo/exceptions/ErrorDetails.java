package com.rest.employee.cruddemo.exceptions;

import java.util.Date;

public class ErrorDetails {

    private Date timesTamp;
    private String message;
    private String details;

    public ErrorDetails(Date timesTamp, String message, String details) {
        super();
        this.timesTamp = timesTamp;
        this.message = message;
        this.details = details;
    }

    public Date getTimesTamp() {
        return timesTamp;
    }

    public void setTimesTamp(Date timesTamp) {
        this.timesTamp = timesTamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}