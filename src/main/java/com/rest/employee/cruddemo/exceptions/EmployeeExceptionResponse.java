package com.rest.employee.cruddemo.exceptions;

public class EmployeeExceptionResponse extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public EmployeeExceptionResponse(String message) {
        super(message);
    }
}